<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreProduct;
use Illuminate\Support\Facades\Auth;
use App\Repositories\ProductRepository;


class ProductController extends Controller
{

    /**
     * @var ProductRepository
     */
    protected $repository;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(ProductRepository $repository)
    {
        $this->middleware('auth:api');
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\ProductRepository  $productRepository
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->repository->paginate(5, ['*'])->toJson();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\StoreProduct  $request
     * @param  \Illuminate\Http\ProductRepository  $productRepository
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProduct $request)
    {
        return $this->repository->storeOrUpdate(null, $request);
    }

    /**
     * Show the resource details.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = $this->repository->find($id);

        return response()->json($product, 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\StoreProduct  $request
     * @param  \Illuminate\Http\ProductRepository  $productRepository
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreProduct $request, $id)
    {
        return $this->repository->storeOrUpdate($id, $request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\ProductRepository  $productRepository
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return $this->repository->destroy($id);
    }
}