<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/products';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * The user has been authenticated.
     *
     * @override AuthenticatesUser.php
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        $accessToken = $user->createToken('LetsTest')->accessToken;
        $request->session()->put('api-token', $accessToken);
    }

    public function logout()
    {
        $userTokens = Auth::user()->tokens;

        //Revoga os tokens de acesso.
        foreach($userTokens as $token) {
            $token->revoke();   
        }

        //Apaga a sessão que contém o token de acesso.
        \Session::flush();

        Auth::logout();
        return redirect('/');
    }
}
