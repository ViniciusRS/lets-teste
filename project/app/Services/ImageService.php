<?php

namespace App\Services;

use ImageOptimizer;


class ImageService
{
	/**
    * Uploads an image from the POST data.
    * @return the uploaded file name or false if the upload fails.
    **/
    public static function upload($image)
    {
        $name = uniqid(date('HisYmd'));
        $extension = $image->extension();
        $file_name = "{$name}.{$extension}";
 
        $upload = $image->storeAs('public/products', $file_name);

        if ($upload)
            return $file_name;

        return false;
    }

    /**
    * Uploads a thumbnail image from the original file from the POST data.
    *
    * @return the uploaded thumbnail file name.
    **/
    public static function uploadThumbnail($image, $original_file_name)
    {
        $thumb_file_name = 'thumb_'.$original_file_name; 

        if (!file_exists('storage/products/thumbnails/')) {
            mkdir('storage/products/thumbnails/', 0777, true);
        }

        //Returns void.
        ImageOptimizer::optimize($image, 'storage/products/thumbnails/'.$thumb_file_name);

        return $thumb_file_name;
    }

}