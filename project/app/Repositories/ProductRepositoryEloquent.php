<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\ProductRepository;
use App\Entities\Product;
use App\Validators\ProductValidator;
use App\Criteria\OrderByCreatedCriteria;
use App\Http\Requests\StoreProduct;
use App\Services\ImageService;
use Illuminate\Support\Facades\File;


/**
 * Class ProductRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class ProductRepositoryEloquent extends BaseRepository implements ProductRepository
{

    protected $fieldSearchable = [
        'title'=>'ilike',
        'description'=>'ilike', // Default Condition "="
        'price'
    ];

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Product::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class)); 
        //Define uma Criteria padrão. Os finds sempre serão ordenados por created_at ASC nesse caso.
        $this->pushCriteria(OrderByCreatedCriteria::class);
    }

    /**
     * Creates or updates a resource in storage.
     *
     * @param  int  $id
     * @param  \App\Http\Requests\StoreProduct  $request
     * @return \Illuminate\Http\Response
     */
    public function storeOrUpdate($id = null, StoreProduct $request) 
    {   
        $image_file_name = null;

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
             
            $image_file_name = ImageService::upload($request->image);

            if ($image_file_name) {
                $thumb_file_name = ImageService::uploadThumbnail($request->file('image'), $image_file_name);
            } else {
                return response()->json(['status' => 'error', 'message' =>  'Upload failrule'], 500);
            }
        } elseif (!$id)
                return response()->json(['status' => 'error', 'message' =>  'Image file is required on create.'], 500);

        $request_data = $request->all();
        $request_data['price'] = number_format((float)$request_data['price'], 2, '.', ''); 

        if ($image_file_name) {
            $request_data['image'] = $image_file_name;
            $request_data['thumbnail'] = $thumb_file_name;
        } else {
          unset($request_data['image']);
          unset($request_data['thumbnail']);
        }
        
        $product = $this->updateOrCreate(['id'=>$id], $request_data);

        if ($product) {
            return response()->json(['status' => 'success', 'product' => $product], 201);
        } 

        return response()->json(['status' => 'error'], 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Repositories\ProductRepository  $productRepository
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = $this->find($id);
        
        $image_file_name = $product->image;
        $thumbnail_file_name = $product->thumbnail;

        if ($this->delete($id)) {

            try {
                File::delete('storage/products/' . $image_file_name);
                File::delete('storage/products/thumbnails/' . $thumbnail_file_name);
            }
            catch (Exception $e) {
                return response()->json(['status' => 'error', 'message' => $e->getMessage()], 500);
            }

            return response()->json(['status' => 'success'], 201);
        }

        return response()->json(['status' => 'error'], 500);
    }
}
                
