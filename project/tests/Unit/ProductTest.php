<?php

namespace Tests;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\User;
use App\Entities\Product;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Tests the index action.
     *
     * @return void
     */
    public function testIndexProducts()
    {
        $user = factory(User::class)->create();

        $response = $this
                        ->actingAs($user)
                        ->get('/products');

        $response->assertStatus(200);
        $response->assertViewIs('product.index');
    }

    /**
     * Tests the store action.
     *
     * @return void
     */
    public function testStoreProduct()
    {
        $user = factory(User::class)->create();

        //Tests product's image upload
    	Storage::fake('local');

        //Tests the product data
        $product = factory(Product::class)->make([
            'image' => UploadedFile::fake()->image('products/product.jpg')
        ]);

        $response = $this
                        ->actingAs($user)
                        ->call('POST', '/api/products', $product->toArray());

        //Asserts
        Storage::disk('public')->assertExists('products/product.jpg');                
        $response->assertStatus(201); 
        $this->assertDatabaseHas('products', [
            'title' => $product->title,
            'description' => $product->description,
            'price' => $product->price
        ]);
    }

    /**
     * Tests the update action.
     *
     * @return void
     */
    public function testUpdateProduct()
    {
        $user = factory(User::class)->create();
        $product = factory(Product::class)->create();

        //Tests the product data
        $updated_data = [
            'title' => $product->title . ' alterado',
            'description' => 'Testing PUT to update an existing product.',
            'price' =>  9,
        ];

        $response = $this
                        ->actingAs($user)
                        ->call('PUT', '/api/products/'.$product->id, $updated_data);

        //Asserts
        $response->assertStatus(201);
        $this->assertDatabaseHas('products', [
            'id' => $product->id,
            'title' => $updated_data['title'],
            'description' => $updated_data['description'],
            'price' => $updated_data['price'],
        ]);
    }

    /**
     * Tests the edit action.
     *
     * @return void
     */
    public function testViewEditProduct()
    {
        $user = factory(User::class)->create();

        $product = factory(Product::class)->create();

        $response = $this
                        ->actingAs($user)
                        ->json('GET', 'api/products/'.$product->id);

        $response->assertStatus(201);
        $response->assertJson([
                'id' => $product->id,
                'title' => $product->title,
                'description' => $product->description,
                'image' => $product->image,
            ]);
    }

    /**
     * Tests the destroy action.
     *
     * @return void
     */
    public function testDeleteProduct()
    {
        $user = factory(User::class)->create();

        $product = factory(Product::class)->create();

        $response = $this
                        ->actingAs($user)
                        ->call('DELETE', '/api/products/'.$product->id, []);

        $response->assertStatus(201);

        $this->assertSoftDeleted('products', [
            'id' => $product->id,
            'title' => $product->title,
            'description' => $product->description,
            'price' => $product->price,
            'image' => $product->image,
            'thumbnail' => $product->thumbnail
        ]);
    }
}