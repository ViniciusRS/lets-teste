<?php

use Illuminate\Http\Request;


Route::middleware('api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('products', 'API\ProductController');


