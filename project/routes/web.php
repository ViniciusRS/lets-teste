<?php

use Illuminate\Http\Request;


Route::resource('/products', 'ProductController');
Route::view('/', 'auth.login');

Auth::routes();

Route::get('/myaccount', 'UserController@myaccount')->name('myaccount');
Route::put('/user/update/{id}', 'UserController@update')->name('users.update');

