<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

$factory->define(App\Entities\Product::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'description' => $faker->text(255),
        'image' => 'product.jpg',
        'thumbnail' => 'thumb_product.jpg',
        'price' => $faker->randomNumber(2),
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
    ];
});
