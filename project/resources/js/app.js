require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';
import Notifications from 'vue-notification';
 
window.Vue.use(VueRouter);
Vue.use(Notifications);

//Componentes importados globalmente
Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue')
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue')
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue')
);

Vue.component('pagination', require('laravel-vue-pagination'));

//Componentes importados localmente.
import ProductsList from './components/products/ProductsList.vue';
import ProductsCreate from './components/products/ProductsCreate.vue';
import ProductsEdit from './components/products/ProductsEdit.vue';

const routes = [
    {
        path: '/',
        components: {
            ProductsList
        }
    },
    {path: '/create', component: ProductsCreate, name: 'createProduct'},
    //O :id significa que é uma rota dinâmica. A rota vai saber substituir o :id pelo id do produto acessado.
    {path: '/edit/:id', component: ProductsEdit, name: 'editProduct'},
];
 
const router = new VueRouter({ routes });

const app = new Vue({
	router,
    el: '#app'
});
