<html>
<head>
	<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{csrf_token()}}">
    <meta name="api-token" content="{{ session('api-token') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">

    @hasSection('css')
		@yield('css')
	@endif

	<title>@yield('title')</title>
</head>
<body>

	<wrapper class="d-flex flex-column">

		@component('component.navbar')
		@endcomponent

		<main role="main" class="container mt-3">
			@hasSection('header_title')

				<div class="pb-2 mt-4 mb-2border-bottom">
				  @hasSection('header_button')
				  	 @yield('header_button')
				  @endif

				  <h1 class="h2">@yield('header_title')</h1>

				  @hasSection('header_subtitle')
				  	<h4 class="h4">@yield('header_subtitle')</h4>
			  	  @endif
				</div>
			@endif

			@hasSection('content')
				<div id="app">	
					@yield('content')	
				</div>
			@endif
		</main>

		@component('component.footer')
		@endcomponent
	</wrapper>

	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="{{asset('js/app.js')}}" type="text/javascript"></script>
	
	@hasSection('scripts')
		@yield('scripts')
	@endif

</body>
</html>