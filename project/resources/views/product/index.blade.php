@extends('layout.base')


@section('title', 'Products - List')

@section('header_title', 'Products')

@section('content')
<div class="card">
  <div class="card-body">  	
    <notifications position='top center'></notifications>

    <div class="panel-body table-responsive">
      <router-view name="ProductsList"></router-view>
      <router-view></router-view>
    </div>
    
  </div>
</div>
@endsection

