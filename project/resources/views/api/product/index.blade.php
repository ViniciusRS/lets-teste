@extends('layout.base')

@section('title', 'Products - List')

@section('header_title', 'Products')
@section('header_subtitle', 'List')


@section('header_button')
  <div class="form-inline float-right mt--1 d-none d-md-flex">
    <a class="btn btn-success" href="/products/create">New product</a>
  </div>
@endsection            

@section('content')

<div class="card">
  <div class="card-body">

    <div id="app" class="panel-body table-responsive">
      <router-view name="productsIndex"></router-view>
      <router-view></router-view> 
    </div>

  </div>
</div>
@endsection

