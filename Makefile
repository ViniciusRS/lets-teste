setup:
	@docker-compose build
	@docker-compose up -d
	@docker-compose run --rm app composer install
	@docker-compose run --rm front npm install
	@docker-compose run --rm app php artisan key:generate
	@docker-compose run --rm app php artisan migrate
	@docker-compose run --rm front npm run dev

clean:
	@docker-compose run --rm front rm -rf node_modules
	@docker-compose run --rm front npm cache clean --force
	@docker-compose down
start:
	@docker-compose stop
	@docker-compose up -d
stop:
	@docker-compose stop
